# Translations template for PROJECT.
# Copyright (C) 2017 ORGANIZATION
# This file is distributed under the same license as the PROJECT project.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2017.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2017-09-22 11:53+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.3.4\n"

#: templates/404.html:28
msgid "Oops! Página no encontrada"
msgstr ""

#: templates/404.html:29
msgid "Lo sentimos. El recurso que ha solicitado no existe."
msgstr ""

#: templates/404.html:30
msgid "Volver al Inicio"
msgstr ""

#: templates/contact.html:31 templates/layout/menu.html:63
#: templates/services/antifraud.html:24 templates/services/audits.html:22
#: templates/services/consultant.html:22 templates/services/koodous.html:22
#: templates/services/sana.html:22 templates/services/training.html:22
msgid "Inicio"
msgstr ""

#: templates/contact.html:32 templates/layout/menu.html:114
#: templates/layout/menu.html:134 templates/layout/menu.html:135
msgid "Contacto"
msgstr ""

#: templates/contact.html:45
msgid "Hable con nosotros"
msgstr ""

#: templates/contact.html:47
msgid "Adaptamos nuestra experiencia a sus necesidades."
msgstr ""

#: templates/contact.html:51 templates/contact.html:89
msgid "Correo electrónico"
msgstr ""

#: templates/contact.html:75
msgid "¿Le contactamos nosotros?"
msgstr ""

#: templates/contact.html:78
msgid "Déjenos un mensaje. Nosotros nos ocuparemos de llamarle."
msgstr ""

#: templates/contact.html:79
msgid "Mensaje recibido. En breve contactaremos con usted."
msgstr ""

#: templates/contact.html:80
msgid ""
"Oops! Perdón. Ha ocurrido un error. Pruebe otra vez, contacte con "
"nosotros en info @ hispasec.com o telefónicamente"
msgstr ""

#: templates/contact.html:84
msgid "Nombre"
msgstr ""

#: templates/contact.html:95
msgid "Teléfono de contacto"
msgstr ""

#: templates/contact.html:100
msgid "Servicio interesado"
msgstr ""

#: templates/contact.html:102 templates/index.html:332
#: templates/layout/menu.html:78 templates/services/antifraud.html:26
#: templates/services/antifraud.html:38
msgid "Antifraude"
msgstr ""

#: templates/contact.html:103 templates/index.html:351
#: templates/layout/menu.html:79 templates/services/audits.html:24
#: templates/services/audits.html:36
msgid "Auditorías"
msgstr ""

#: templates/contact.html:106 templates/index.html:472
#: templates/layout/menu.html:84 templates/services/training.html:24
#: templates/services/training.html:38
msgid "Formación"
msgstr ""

#: templates/contact.html:109
msgid "Otros"
msgstr ""

#: templates/contact.html:113
msgid "Mensaje"
msgstr ""

#: templates/contact.html:120
msgid "Quiero recibir información sobre nuevos productos y ofertas"
msgstr ""

#: templates/contact.html:162
msgid "Correos específicos de cada departamento:"
msgstr ""

#: templates/contact.html:165
msgid "Información general:"
msgstr ""

#: templates/contact.html:166
msgid "Laboratorio Técnico:"
msgstr ""

#: templates/contact.html:167
msgid "Comercial:"
msgstr ""

#: templates/contact.html:168
msgid "Medios de comunicación:"
msgstr ""

#: templates/contact.html:169
msgid "Recursos Humanos:"
msgstr ""

#: templates/contact.html:183
msgid "Ver ubicación"
msgstr ""

#: templates/copyright.html:43
msgid ""
"Todas las noticias Hispasec una-al-dia están protegidas por las leyes del"
" copyright."
msgstr ""

#: templates/copyright.html:45
msgid "Se permite la copia y utilización para usos <strong>privados.</strong>"
msgstr ""

#: templates/copyright.html:46
msgid ""
"Para uso <strong>público y no lucatrivo,</strong> se permite la "
"reproducción íntegra y sin alteraciones de este documento, con mención de"
" autor y URL completa."
msgstr ""

#: templates/copyright.html:47
msgid ""
"Se <strong>prohíbe expresamente</strong> la copia o utilización para uso "
"lucrativo y la copia o utilización parcial de su contenido sin expresa "
"autorización, por escrito, de su autor o autores."
msgstr ""

#: templates/copyright.html:50
msgid "En caso de duda consulte a "
msgstr ""

#: templates/hispasec.html:39
msgid "Origen de"
msgstr ""

#: templates/hispasec.html:43
msgid ""
"Hispasec nace a finales de 1998 con el lanzamiento de 'una-al-día', el "
"primer servicio diario de información técnica en español sobre seguridad "
"informática, creado por un grupo de especialistas con el propósito de "
"divulgar y concienciar a los usuarios de la importancia de este sector en"
" el campo de las nuevas tecnologías de la información."
msgstr ""

#: templates/hispasec.html:44
msgid ""
"El éxito cosechado por 'una-al-día' y el resto de acciones públicas "
"emprendidas por Hispasec (análisis, comparativas, desarrollos, etc.) "
"origina, de forma espontánea, una demanda de servicios por parte de "
"profesionales y empresas."
msgstr ""

#: templates/hispasec.html:45
msgid ""
"Para responder a esta demanda, se crea el laboratorio de seguridad "
"informática, Hispasec, en el año 2000 . Hispasec es una empresa de "
"marcado carácter técnico, sin capital externo ni supeditación a intereses"
" ajenos, gestionada íntegramente por los fundadores de Hispasec."
msgstr ""

#: templates/hispasec.html:46
msgid ""
"El punto de inflexión para Hispasec fue el posterior desarrollo de "
"Virustotal que sirvió de inspiración para el desarrollo de nuevos "
"proyectos de investigación y desarrollo. Actualmente nuestro buque "
"insignia es el proyecto Koodous."
msgstr ""

#: templates/hispasec.html:48
msgid "Profesionales experimentados de la seguridad digital"
msgstr ""

#: templates/hispasec.html:49
msgid "Equipos multidisciplinares de expertos"
msgstr ""

#: templates/hispasec.html:50
msgid "Inversión continua en formación y desarrollo"
msgstr ""

#: templates/hispasec.html:51
msgid "Servicios 24x7, guardia permanente"
msgstr ""

#: templates/index.html:44 templates/index.html:57
#: templates/layout/menu.html:73
msgid "HISPASEC | Servicios de seguridad"
msgstr ""

#: templates/index.html:67
msgid "Servicios activos 24x7, 365 días al año."
msgstr ""

#: templates/index.html:77
msgid "Un equipo de técnicos expertos al mando."
msgstr ""

#: templates/index.html:87
msgid "Innovación y desarrollo permanente de nuevos servicios."
msgstr ""

#: templates/index.html:97
msgid "Seguridad flexible y adaptativa."
msgstr ""

#: templates/index.html:107 templates/index.html:188 templates/index.html:260
#: templates/index.html:336 templates/index.html:355 templates/index.html:374
#: templates/index.html:444 templates/index.html:460 templates/index.html:476
#: templates/services/antifraud.html:63 templates/services/antifraud.html:98
#: templates/services/audits.html:103 templates/services/audits.html:111
#: templates/services/audits.html:140 templates/services/koodous.html:59
#: templates/services/koodous.html:68 templates/services/koodous.html:77
msgid "Más información"
msgstr ""

#: templates/index.html:126 templates/index.html:138
msgid "Koodous | La comunidad de análisis Android colaborativa"
msgstr ""

#: templates/index.html:148
msgid "Análisis libre y colaborativo de aplicaciones de Android"
msgstr ""

#: templates/index.html:158
msgid "Defina sus propias reglas y alertas."
msgstr ""

#: templates/index.html:168
msgid "API accesible para integrar en sus sistemas."
msgstr ""

#: templates/index.html:178
msgid "Comunidad en continuo crecimiento."
msgstr ""

#: templates/index.html:208 templates/index.html:220
msgid "IOCflow"
msgstr ""

#: templates/index.html:230
msgid "Información en tiempo real sobre una gran variedad de amenazas"
msgstr ""

#: templates/index.html:240
msgid "Proteja a sus usuarios contra amenazas"
msgstr ""

#: templates/index.html:250
msgid "Alimente sus sistemas IDS con la última información"
msgstr ""

#: templates/index.html:298
msgid "Recibe Una al Día en tu correo"
msgstr ""

#: templates/index.html:301
msgid "Suscríbeme"
msgstr ""

#: templates/index.html:319
msgid "Nuestros"
msgstr ""

#: templates/index.html:319 templates/layout/menu.html:68
#: templates/services/antifraud.html:25 templates/services/audits.html:23
#: templates/services/consultant.html:23 templates/services/koodous.html:23
#: templates/services/sana.html:23 templates/services/training.html:23
msgid "Servicios"
msgstr ""

#: templates/index.html:328 templates/layout/menu.html:78
msgid "Servicio 24x7 de detección y cierre de incidentes de fraude"
msgstr ""

#: templates/index.html:334
msgid ""
"Servicio dedicado a neutralizar el malware, daño de imagen o fraude de "
"marca que pueda afectarle. Todo gestionado integramente por nuestro "
"departamento eCrime 24x7x365."
msgstr ""

#: templates/index.html:347
msgid "Cada auditoría, un reto."
msgstr ""

#: templates/index.html:353
msgid ""
"El equipo de auditoria tiene una motivación: Reconocer, analizar y "
"explotar de manera minuciosa su infraestructura Para nuestros auditores "
"no solo es un trabajo, es un reto constante."
msgstr ""

#: templates/index.html:366 templates/layout/menu.html:80
msgid "Que no se te pase ninguna actualización de tus sistemas."
msgstr ""

#: templates/index.html:372
msgid ""
"Concentre las alertas de seguridad, ponga remedio a las vulnerabilidades "
"en el mínimo tiempo posible. Deje a nosotros su obligación de estar "
"pendiente  de los agujeros de seguridad de sus productos."
msgstr ""

#: templates/index.html:383 templates/index.html:420
msgid "Blog"
msgstr ""

#: templates/index.html:394 templates/index.html:404 templates/index.html:414
#: templates/index.html:493 templates/index.html:502 templates/index.html:511
msgid "Leer más"
msgstr ""

#: templates/index.html:420
msgid "del Laboratorio"
msgstr ""

#: templates/index.html:436
msgid "Comunidad de analistas de todo el mundo expertos en Android."
msgstr ""

#: templates/index.html:442
msgid ""
"Millones de aplicaciones para analizar y crear alertas según sus "
"necesidades. Totalmente libre y adaptable gracias a su API."
msgstr ""

#: templates/index.html:452
msgid "Mantente a la vanguardia de la ciberinteligencia."
msgstr ""

#: templates/index.html:458
msgid ""
"Servicio que ofrece feeds útiles para reforzar sus sistemas contra "
"futuras amenazas"
msgstr ""

#: templates/index.html:468 templates/layout/menu.html:84
msgid "Formación a medida de seguridad informática"
msgstr ""

#: templates/index.html:474
msgid ""
"Ofrecemos formación en seguridad multinivel y cursos personalizados para "
"empleados y directivos. Elija su próximo conocimiento."
msgstr ""

#: templates/layout/footer.html:32
msgid "Todos los derechos reservados."
msgstr ""

#: templates/layout/footer.html:35
msgid "Correos de interés"
msgstr ""

#: templates/layout/footer.html:46
msgid ""
"utiliza cookies propias y de terceros para garantizar el correcto "
"funcionamiento de su sitio web. Si continua navegando se considera "
"aceptado el uso de cookies."
msgstr ""

#: templates/layout/footer.html:47
msgid "cerrar"
msgstr ""

#: templates/layout/menu.html:25
msgid "Seguridad. Experiencia. Innovación."
msgstr ""

#: templates/layout/menu.html:79
msgid "Cada auditoría, un reto"
msgstr ""

#: templates/layout/menu.html:81
msgid "Comunidad de analistas de todo el mundo expertos en Android"
msgstr ""

#: templates/layout/menu.html:82 templates/services/consultant.html:24
#: templates/services/consultant.html:39
msgid "Consultoría"
msgstr ""

#: templates/layout/menu.html:82
msgid "Servicio integral de consultoría"
msgstr ""

#: templates/layout/menu.html:83
msgid "Mantente a la vanguardia de la ciberinteligencia"
msgstr ""

#: templates/layout/menu.html:90
msgid "Descubra nuestros servicios para su entidad o empresa"
msgstr ""

#: templates/layout/menu.html:91
msgid ""
"Desde el malware hasta las auditorías, nos encargamos de todo el proceso "
"de seguridad."
msgstr ""

#: templates/layout/menu.html:105
msgid "Acerca de"
msgstr ""

#: templates/layout/menu.html:120
msgid "Blogs"
msgstr ""

#: templates/layout/menu.html:123
msgid "Laboratorio"
msgstr ""

#: templates/layout/menu.html:137 templates/layout/menu.html:138
msgid "Acceso clientes"
msgstr ""

#: templates/services/antifraud.html:38 templates/services/koodous.html:35
#: templates/services/sana.html:36
msgid "Servicio"
msgstr ""

#: templates/services/antifraud.html:40
msgid ""
"Antifraude integra los servicios de Antiphishing, Antitroyanos y Análisis"
" forense en un CSIRT propio y compuesto por un equipo de profesionales en"
" guardia 24x7 con el fin de atender sus incidencias."
msgstr ""

#: templates/services/antifraud.html:60
msgid "CSIRT propio 24/7"
msgstr ""

#: templates/services/antifraud.html:62
msgid ""
"El uso fraudulento de su imagen y marca puede producirse en cualquier "
"momento.  El CSIRT de Hispasec vela por la detección, evaluación y "
"neutralización de  cualquier amenaza vinculada a su organización. 24 "
"horas al día, 7 días a la semana. Sin interrupciones."
msgstr ""

#: templates/services/antifraud.html:69
msgid "Antiphishing"
msgstr ""

#: templates/services/antifraud.html:71
msgid ""
"Nuestra experiencia de años en la lucha contra las campañas de phishing "
"nos ha permitido desarrollar una red de sensores única, con capacidad de "
"detección temprana y respuesta contundente para garantizarla "
"neutralización de esta amenaza en el menor tiempo posible."
msgstr ""

#: templates/services/antifraud.html:77
msgid "Antitroyanos"
msgstr ""

#: templates/services/antifraud.html:79
msgid ""
"El malware es una amenaza que infecta a sus clientes y daña la reputación"
" de su organización. Hispasec posee un equipo propio y dedicado en "
"exclusiva a la detección, análisis y erradicación del malware orientado "
"al fraude."
msgstr ""

#: templates/services/antifraud.html:87
msgid "Análisis Forense"
msgstr ""

#: templates/services/antifraud.html:89
msgid ""
"Gracias al estudio de los sistemas infectados, se puede conocer en "
"detalle el vector de ataque que el malware está aprovechando para "
"infectar a sus clientes o los equipos de su organización.El conocimiento "
"de una nueva amenaza le permitirá elegir la mejor opción para defenderse."
msgstr ""

#: templates/services/antifraud.html:95 templates/services/koodous.html:56
msgid "Seguridad Android"
msgstr ""

#: templates/services/antifraud.html:97
msgid ""
"Desde nuestro proyecto colaborativo Koodous y junto con el equipo de "
"analistas de malware, podemos ofrecer un servicio completo de detección "
"temprana y gestión de amenazas en los sistemas operativos Android."
msgstr ""

#: templates/services/antifraud.html:104 templates/services/koodous.html:65
msgid "API personalizable"
msgstr ""

#: templates/services/antifraud.html:106
msgid ""
"Todos nuestros servicios ofrecen una API configurable y adaptable a sus "
"necesidades, para así poder implantarlas en sus sistemas. Flexibilidad. "
"Nosotros aportamos el conocimiento y los datos a su visión"
msgstr ""

#: templates/services/antifraud.html:123
msgid "Servicios complementarios a Antifraude"
msgstr ""

#: templates/services/antifraud.html:125
msgid ""
"Para complementar la detección y gestión de casos de Phishing, Troyanos "
"y/o Forenses, HISPASEC pone a su disposición servicios complementarios "
"para necesidades muy específicas."
msgstr ""

#: templates/services/antifraud.html:133
msgid ""
"El servicio LSI, ofrece un análisis automatizado 24/7 de los registros "
"generados por los servicios web críticos a nivel de peticiones. El "
"objetivo es la detección temprana de actividades sospechosas que "
"evidencien el abuso contra su organización (Phishing, Malware y abuso de "
"marca)."
msgstr ""

#: templates/services/antifraud.html:140
msgid "Reputación y vigilancia de marca"
msgstr ""

#: templates/services/antifraud.html:141
msgid ""
"A través de técnicas OSINT (Open Source Intelligence) y Machine Learning "
"podemos detectar un uso fraudulento o influencia social sobre la "
"reputación de su organización o marcas comerciales. Servicio de detección"
" y clasificación léxica automatizada personalizable a través de "
"parámetros."
msgstr ""

#: templates/services/antifraud.html:149
msgid "ITW"
msgstr ""

#: templates/services/antifraud.html:150
msgid ""
"El servicio ITW realiza un seguimiento de su web monitorizando su "
"certificado, buscando resoluciones incorrectas y notificando de posibles "
"defacements."
msgstr ""

#: templates/services/antifraud.html:159
msgid "DMARC"
msgstr ""

#: templates/services/antifraud.html:160
msgid ""
"DMARC es un estándar por el cual, cualquier servidor de correo podrá "
"enviar un reporte en caso de email spoofing que será analizado por "
"nuestros sistemas. Estos reportes contienen información que nos ayuda en "
"la detección temprana de casos de phishing contra la entidad de nuestros "
"clientes."
msgstr ""

#: templates/services/antifraud.html:186 templates/services/audits.html:135
#: templates/services/consultant.html:115 templates/services/sana.html:181
#: templates/services/training.html:113
msgid "¿Interesado?"
msgstr ""

#: templates/services/antifraud.html:187 templates/services/audits.html:136
#: templates/services/sana.html:182
msgid ""
"Nuestros servicios tienen disponible un periodo de demostración.Ponganos "
"a prueba. Rétenos. Para HISPASEC el trabajo es una oportunidadpara poder "
"competir."
msgstr ""

#: templates/services/antifraud.html:191 templates/services/sana.html:186
msgid "Solicite una DEMO"
msgstr ""

#: templates/services/antifraud.html:209
msgid "Nuestro cuadro de mandos CSIRT"
msgstr ""

#: templates/services/antifraud.html:211
msgid ""
"Cuadro de mandos configurable. Obtenga una visión integral del estado de "
"su seguridad.Consulte el pasado, estudie el presente y prepare su "
"estrategia de seguridad para el futuro."
msgstr ""

#: templates/services/antifraud.html:218
msgid "Gestión de cuentas"
msgstr ""

#: templates/services/antifraud.html:225
msgid "Gestión de entidades y roles de usuarios"
msgstr ""

#: templates/services/antifraud.html:227
msgid ""
"Tendrá un control total de los usuarios que acceden al panel, sus "
"permisos, registros y acciones relacionadas."
msgstr ""

#: templates/services/antifraud.html:239
msgid "Listado global y estadísticas"
msgstr ""

#: templates/services/antifraud.html:246
msgid "Todos sus datos. Al alcance de un click."
msgstr ""

#: templates/services/antifraud.html:248
msgid ""
"Podrá gestionar cada caso de manera independiente, viendo en todo momento"
" su estado y proceso de cierre, así como las estadísticas mensuales."
msgstr ""

#: templates/services/antifraud.html:260
msgid "Histórico y filtrado"
msgstr ""

#: templates/services/antifraud.html:267
msgid "Histórico de casos"
msgstr ""

#: templates/services/antifraud.html:269
msgid ""
"Podrá acceder al listado completo y filtrar por diferentes estados de "
"caso o fechas entre otras modalidades."
msgstr ""

#: templates/services/antifraud.html:281
msgid "Acciones realizadas en el incidente"
msgstr ""

#: templates/services/antifraud.html:288
msgid "Detalle de incidente"
msgstr ""

#: templates/services/antifraud.html:290
msgid ""
"En cada caso gestionado, se establece una correlación de las acciones "
"tomadas para su cierre completo."
msgstr ""

#: templates/services/audits.html:36
msgid "Servicio de"
msgstr ""

#: templates/services/audits.html:38
msgid ""
"¿Cómo ve un grupo de atacantes la infraestructura tecnológica de su "
"organización?¿De que forma puede medir el estado de sus defensas? El "
"mejor método para comprobar su seguridad es comprometiéndola de manera "
"ética."
msgstr ""

#: templates/services/audits.html:58
msgid "¿Red Team o Blue Team?"
msgstr ""

#: templates/services/audits.html:60
msgid ""
"El equipo de auditorías de Hispasec lo forman profesionales de la "
"seguridad con experiencia y una larga trayectoria en el sector. "
"Integrantes del equipo poseen certificaciones de seguridad y formación "
"especializada para el ejercicio de sus funciones."
msgstr ""

#: templates/services/audits.html:66
msgid "Métodos y espíritu de trabajo"
msgstr ""

#: templates/services/audits.html:68
msgid ""
"Una auditoría no solo es un trabajo meticuloso y de gran responsabilidad,"
" es un reto que asumimos, un desafío lógico que pone a prueba nuestra "
"destreza.Nuestro concepto de trabajo abraza metodologías como OWASP o "
"OSTMM dejando espacio para la creatividad y el trabajo artesano. No nos "
"detenemos en la interpretaciónde los datos producidos por herramientas "
"automatizadas. Conectar los puntos, pensar de manera lateral y reenfocar "
"los problemas haciendo que nuevas ópticas produzcan perspectivas únicas "
"para cada desafío que nos encomiendan."
msgstr ""

#: templates/services/audits.html:74
msgid "Un problema, una solución"
msgstr ""

#: templates/services/audits.html:76
msgid ""
"No basta con señalar un problema, una vulnerabilidad o una deficiencia.El"
" siguiente paso es corregirlo y no le dejamos sólo en el proceso.Nuestra "
"misión no acaba con la entrega de informes personalizados, análisis "
"comparativos o resúmenes ejecutivos.Estudiemos conjuntamente una "
"estrategia de planificación. Personalizemos una solución concreta para un"
" problema determinado. No trabajemos frente a frente, resérvenos un sitio"
" su lado."
msgstr ""

#: templates/services/audits.html:94
msgid "Tipos de auditorías disponibles"
msgstr ""

#: templates/services/audits.html:96
msgid ""
"Cada necesidad requiere una atención diferente.Estas son nuestras "
"propuestas:"
msgstr ""

#: templates/services/audits.html:102
msgid "Técnicas"
msgstr ""

#: templates/services/audits.html:103
msgid "Auditorías técnicas, Test de penetración, auditoría WiFi..."
msgstr ""

#: templates/services/audits.html:110
msgid "Gestión"
msgstr ""

#: templates/services/audits.html:111
msgid ""
"Engloba aquellas encargadas de auditar todo el proceso SGSI de la empresa"
" y específicamente la norma 27001."
msgstr ""

#: templates/services/audits.html:155
msgid "Auditorías técnicas"
msgstr ""

#: templates/services/audits.html:162
msgid "Auditoría perimetral"
msgstr ""

#: templates/services/audits.html:163
msgid ""
"Evalúa la seguridad de los activos que la organización mantiene "
"publicados en Internet. Permite conocer cómo ve un atacante externo los "
"servidores de la empresa, qué impacto puede tener un eventual ataque y "
"qué medidas han de implementarse para corregir las deficiencias de "
"seguridad."
msgstr ""

#: templates/services/audits.html:170
msgid "Test de intrusión (Pentest)"
msgstr ""

#: templates/services/audits.html:171
msgid ""
"Este tipo de pruebas tiene como objeto conocer hasta dónde llegaría un "
"atacante externo si fijase a la organización en su punto de mira. Permite"
" conocer si es posible encadenar la explotación de vulnerabilidades y "
"brechas en las políticas de seguridad para adentrarse en los sistemas de "
"información de la empresa."
msgstr ""

#: templates/services/audits.html:178
msgid "Auditoría WiFi"
msgstr ""

#: templates/services/audits.html:179
msgid ""
"Nos permite conocer los puntos débiles de la red inalámbrica. Examina la "
"implementación realizada y analiza los problemas que pueden surgir de una"
" mala configuración. Adicionalmente también se analizan los posibles "
"puntos de acceso no autorizados. Es decir, aquellos accesos inalámbricos "
"instalados sin la autorización de la organización y que podrían servir de"
" puerta de acceso a un atacante."
msgstr ""

#: templates/services/audits.html:186
msgid "Auditoría de código"
msgstr ""

#: templates/services/audits.html:187
msgid ""
"Análisis del código tanto de aplicaciones de páginas Web como de "
"cualquier tipo de aplicación nativa, independientemente del lenguaje "
"empleado."
msgstr ""

#: templates/services/audits.html:197
msgid "Auditoría interna"
msgstr ""

#: templates/services/audits.html:198
msgid ""
"Evalúa la seguridad de la red interna de la empresa. Permite conocer "
"hasta dónde podría llegar un usuario malicioso con credenciales o desde "
"una posición con mayores privilegios. En definitiva, se trata de "
"determinar el daño que podría ser causado desde dentro de la "
"organización."
msgstr ""

#: templates/services/audits.html:205
msgid "Auditoría de aplicaciones Web"
msgstr ""

#: templates/services/audits.html:206
msgid ""
"Este tipo de auditoría se especializa en las aplicaciones Web. Debido a "
"la complejidad y criticidad de los datos y operaciones que procesan, las "
"aplicaciones Web precisan de un tratamiento único y diferenciado que "
"tenga por objeto neutralizar las vulnerabilidades existentes."
msgstr ""

#: templates/services/audits.html:212
msgid "Auditoría de aplicaciones móviles"
msgstr ""

#: templates/services/audits.html:213
msgid ""
"Revisión y análisis de las aplicaciones móviles Android, iOS, y Windows "
"Phone. Examen de las comunicaciones, configuración, debilidades e "
"interacción con las funcionalidades del sistema."
msgstr ""

#: templates/services/audits.html:230
msgid "Auditorías de Gestión"
msgstr ""

#: templates/services/audits.html:238
msgid "Proceso de auditoría SGSI"
msgstr ""

#: templates/services/audits.html:244
msgid ""
"Partiendo de la base de que garantizar un nivel de protección total es "
"imposible, incluso en el caso de disponer de un presupuesto ilimitado, el"
" propósito de un SGSI es el de garantizar que los riesgos de la seguridad"
" de la información son conocidos, asumidos, gestionados y minimizados por"
" la organización de una forma documentada, sistemática, estructurada, "
"repetible, eficiente y adaptada a los cambios que se produzcan en los "
"riesgos, el entorno y las tecnologías. Para ello, el SGSI se basa en "
"estándares internacionales como la norma"
msgstr ""

#: templates/services/audits.html:245
msgid ""
"La implantación de SGSI es una actividad de creciente demanda en nuestro "
"mercado y las empresas comienzan a entender su importancia, al igual que "
"ha ocurrido con la implantación de sistemas de gestión de la Calidad (ISO"
" 9000) o Medio Ambiente (ISO 14000) en los últimos años."
msgstr ""

#: templates/services/consultant.html:41
msgid ""
"Nuestros diferentes departamentos y gracias a nuestra trayectoria, nos "
"permiten asesoran en multiples disciplinas de seguridad y así ofrecer un "
"servicio completo de consultoría para su empresa"
msgstr ""

#: templates/services/consultant.html:42 templates/services/training.html:41
msgid "Experiencia"
msgstr ""

#: templates/services/consultant.html:43
msgid ""
"HISPASEC ofrece un amplio departamento técnico y consultivo con los "
"mejores profesionales."
msgstr ""

#: templates/services/consultant.html:44
msgid ""
"Basados en los estándares y mejores prácticas de la industria, brindamos "
"asistencia en análisis de riesgos tecnológicos, desarrollo de planes de "
"recuperación ante desastres e implementación de planes de contingencia."
msgstr ""

#: templates/services/consultant.html:76
msgid "Seguridad en la red empresarial"
msgstr ""

#: templates/services/consultant.html:78
msgid "Asesoramiento en los diferentes departamentos y redes de la empresa."
msgstr ""

#: templates/services/consultant.html:84
msgid "Protección de datos"
msgstr ""

#: templates/services/consultant.html:86
msgid ""
"Orientado a todo el proceso relacionado con la gestión y protección de "
"los datos privados de la entidad."
msgstr ""

#: templates/services/consultant.html:92
msgid "Seguridad en comunicaciones inalámbricas"
msgstr ""

#: templates/services/consultant.html:94
msgid ""
"Asesoramiento sobre el correcto dimensionamiento de las infraestucturas "
"WIFI implantadas para evitar problemas relacionados con la seguridad."
msgstr ""

#: templates/services/consultant.html:116 templates/services/training.html:114
msgid ""
"Nuestro departamento se pondrá en contacto con ustedes lo antes posible. "
"No dude en consultarlo con nosotros"
msgstr ""

#: templates/services/consultant.html:120 templates/services/training.html:118
msgid "Consúltenos"
msgstr ""

#: templates/services/koodous.html:37
msgid ""
"Millones de aplicaciones para analizar y crear alertas según sus "
"necesidades. Totalmente libre y adaptable gracias a su API. Disponible "
"aplicación móvil de detección y análisis de APKs instalados."
msgstr ""

#: templates/services/koodous.html:58
msgid ""
"Gracias a nuestro proyecto colaborativo Koodous y a nuestro grupo de "
"analistas en malware, podemos ofrecer un servicio completo de detección "
"temprana y gestión de amenazas en Android."
msgstr ""

#: templates/services/koodous.html:67
msgid ""
"Todos nuestros servicios ofrecen una API configurable y adaptable a sus "
"necesidades, para así poder implantarlas en sus sistemas."
msgstr ""

#: templates/services/koodous.html:74
msgid "APP móvil"
msgstr ""

#: templates/services/koodous.html:76
msgid "Aplicación móvil de detección de APKs instalados y su nivel de seguridad."
msgstr ""

#: templates/services/sana.html:38
msgid ""
"Servicio de Análisis, Notificación y Alertas. Nuestro sistema de alertas "
"de seguridad, que le permite estar al tanto de cualquier incidencia "
"relacionada con sus sistemas o integrarse en sus aplicaciones."
msgstr ""

#: templates/services/sana.html:49
msgid ""
"El Servicio de Análisis, Notificación y Alertas (SANA) forma parte de la "
"historia de Hispasec como uno de los servicios más longevos y mejor "
"considerados por nuestros clientes. SANA proporciona un sistema completo "
"de alertas sobre vulnerabilidades, actualizaciones y parches de software "
"en términos de seguridad informática, permitiendo saber en todo momento "
"las posibles vulnerabilidades que puedan afectar a los productos que su "
"entidad o empresa usan a diario."
msgstr ""

#: templates/services/sana.html:56
msgid "Alerta temprana de vulnerabilidades"
msgstr ""

#: templates/services/sana.html:57
msgid ""
"Cada cliente recibe de manera rápida y personalizada tantas alertas como "
"fallos hayan sido detectados para los productos monitorizados. Para "
"sistemas más complejos, disponemos de una completa API que permite "
"personalizar y adaptar este servicio a sus necesidades."
msgstr ""

#: templates/services/sana.html:58
msgid ""
"En cada alerta se proporciona toda la información necesaria así como los "
"parches disponibles en ese momento o las posibles contramedidas, todo "
"ello en un servicio activo 24/7."
msgstr ""

#: templates/services/sana.html:65
msgid "Histórico completo"
msgstr ""

#: templates/services/sana.html:66
msgid ""
"Informe del histórico de vulnerabilidades para cada cliente, donde "
"consultar de manera cómoda las vulnerabilidades que le afectan, "
"permitiendo consultas en base a cualquier característica de la alerta."
msgstr ""

#: templates/services/sana.html:67
msgid ""
"El portal, también permite catalogar las vulnerabilidades a los "
"administradores, de forma que puedan conocer qué fallos han sido "
"corregidos en su sistema, están pendientes de gestionar, o aún no han "
"sido tratados."
msgstr ""

#: templates/services/sana.html:68
msgid ""
"Todo ello integrado en nuestro panel de servicios CSIRT, ofreciendo "
"estadísticas completas para presentaciones ejecutivas."
msgstr ""

#: templates/services/sana.html:96
msgid "Nuestro panel SANA"
msgstr ""

#: templates/services/sana.html:98
msgid ""
"Nuestro panel es totalmente configurable a sus necesidades, mostrando de "
"un vistazo todos los alertas gestionados y/o emitidas."
msgstr ""

#: templates/services/sana.html:105
msgid "Dashboard completo"
msgstr ""

#: templates/services/sana.html:112
msgid "Estadísticas de vulnerabilidades"
msgstr ""

#: templates/services/sana.html:114
msgid ""
"De un vistazo podrá controlar el total de alertas sufridas, así como el "
"calendario de estadísticas."
msgstr ""

#: templates/services/sana.html:126
msgid "Listado global y búsqueda"
msgstr ""

#: templates/services/sana.html:133
msgid "Búsqueda de vulnerabilidades"
msgstr ""

#: templates/services/sana.html:135
msgid "Total de alertas gestionadas y búsqueda con diferentes filtros."
msgstr ""

#: templates/services/sana.html:147
msgid "Control de usuarios y productos"
msgstr ""

#: templates/services/sana.html:154
msgid "Gestión de productos a monitorizar"
msgstr ""

#: templates/services/sana.html:156
msgid ""
"Podrá acceder al listado completo y agregar los que necesite para poder "
"ser alertado."
msgstr ""

#: templates/services/training.html:40
msgid ""
"Con mayor frecuencia, se está tomando conciencia de la importancia de "
"mantener la información de la empresa segura y a salvo. La información es"
" un flujo dinámico en continuo movimiento que necesita vigilancia y "
"bastionado en todos sus procesos. Esta necesidad ha llevado a HISPASEC a "
"realizar este curso donde trataremos de dar un enfoque práctico de las "
"amenazas y bastionados de nuestros sistemas y plataformas web."
msgstr ""

#: templates/services/training.html:42
msgid ""
"HISPASEC cuenta con una amplia experiencia de más de 16 años en el "
"sector, siendo un interlocutor del sector activo mediante la publicación "
"del boletín de información \"Una al día\" donde informamos al público en "
"general y a los profesionales en particular de lo que ocurre en el mundo "
"de la seguridad desde una óptica fundamentalmente técnica."
msgstr ""

#: templates/services/training.html:43
msgid ""
"Adicionalmente es conocida nuestra participación y colaboración en "
"congresos, universidades y distintos proyectos Europeos. Esta experiencia"
" unifica tanto la investigación como la realidad de lo que ocurre en la "
"empresa gracias a nuestra experiencia como auditores."
msgstr ""

#: templates/services/training.html:74
msgid "Seguridad en la empresa"
msgstr ""

#: templates/services/training.html:76
msgid ""
"Concienciación de la seguridad para distintos ámbitos de la empresa y "
"dirigidos a todo tipo de personal de la misma."
msgstr ""

#: templates/services/training.html:82
msgid "Malware, seguridad y bastionado"
msgstr ""

#: templates/services/training.html:84
msgid ""
"Orientado a administradores de sistemas y profesionales de la seguridad, "
"permite conocer las amenazas actuales y como proteger sus sistemas "
"debidamente."
msgstr ""

#: templates/services/training.html:90
msgid "Auditoría web o sistemas"
msgstr ""

#: templates/services/training.html:92
msgid ""
"Orientado a administradores de sistemas y profesionales de la seguridad, "
"permite conocer las vulnerabilidades típicas e identificar los puntos "
"flacos de los sistemas para poder adelantarse ante cualquier intrusión."
msgstr ""

